TEMPLATE = subdirs
SUBDIRS  = src

unix {
    QMAKE_CXXFLAGS += -fPIC -fvisibility=hidden -fvisibility-inlines-hidden
    QMAKE_LFLAGS += -pie -rdynamic
    QT += dbus

    INSTALLS += iconpng iconsvg desktopfile
    iconpng.files = installables/meevatr.png
    iconpng.path = /usr/share/icons/hicolor/80x80/apps
    iconsvg.files = installables/meevatr.svg
    iconsvg.path = /usr/share/icons/hicolor/scalable/apps
    desktopfile.files = installables/meevatr.desktop
    desktopfile.path = /usr/share/applications
}
