/*****************************************************************************
 *
 * This file is part of MeeVaTr, a Meego/Harmattan program to fetch
 * time tables from the West-Swedish VästTrafik public transportation system.
 *
 *  Copyright (C) 2012  Thomas Fischer <fischer@unix-ag.uni-kl.de>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QGraphicsLinearLayout>
#include <QTextStream>
#include <QDebug>
#include <QTimer>

#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkRequest>
#include <QtNetwork/QNetworkReply>

#include <MLabel>
#include <MPannableViewport>
#include <MMessageBox>
#include <MAction>
#include <MProgressIndicator>

#include "departure.h"
#include "departureboard.h"

DepartureWidget::DepartureWidget(QSharedPointer<StopLocation> &stopLocation, QGraphicsItem *parent)
    : MWidget(parent), m_stopLocation(stopLocation)
{
    m_networkAccessManager = new QNetworkAccessManager(this);
    m_networkBusy = false;
    setupGUI();
}

void DepartureWidget::createContent()
{
    /// Use delayed initialization to avoid flickering
    QTimer::singleShot(100, this, SLOT(delayedInit()));
}

void DepartureWidget::searchFinished()
{
    QNetworkReply *reply = static_cast<QNetworkReply *>(sender());

    m_progressbar->setVisible(false);

    if (reply->error() == QNetworkReply::NoError) {
        QTextStream ts(reply->readAll());
        ts.setCodec("UTF-8");
        const QString xml = ts.readAll();
        m_departureBoard->setXMLdata(xml);
    } else {
        m_departureBoard->clear();
        qWarning() << "Search Failed: error code" << reply->error();
        MMessageBox *mb = new MMessageBox(QLatin1String("Search Failed"), QString::fromUtf8("Could not search VästTrafik.\n\nError code: %1").arg(reply->error()), M::CancelButton);
        mb->exec();
    }

    setEnabled(true);
    m_networkBusy = false;
}

void DepartureWidget::refreshRequested()
{
    if (m_networkBusy) return; /// ignore multiple class while busy
    m_networkBusy = true;

    setEnabled(false);
    m_progressbar->setVisible(true);

    QDateTime now = QDateTime::currentDateTime();
    QUrl url("http://api.vasttrafik.se/bin/rest.exe/v1/departureBoard");
    url.addQueryItem("authKey", "47c5abaf-49d6-4c23-a1bd-b2e2766c4de7");
    url.addQueryItem("format", "xml");
    url.addQueryItem("date", now.toString("yyyy-MM-dd"));
    url.addQueryItem("time", now.toString("hh:mm"));
    url.addQueryItem("id", QString::number(m_stopLocation->id));
    url.addQueryItem("useVas", "1");
    url.addQueryItem("useLDTrain", "0");
    url.addQueryItem("useRegTrain", "1");
    url.addQueryItem("useBus", "1");
    url.addQueryItem("useBoat", "1");
    url.addQueryItem("useTram", "1");
    QNetworkReply *reply = m_networkAccessManager->get(QNetworkRequest(url));
    connect(reply, SIGNAL(finished()), this, SLOT(searchFinished()));
}

void DepartureWidget::delayedInit()
{
    refreshRequested();
}

void DepartureWidget::setupGUI()
{
    QGraphicsLinearLayout *vLayout = new QGraphicsLinearLayout(Qt::Vertical, this);

    qreal left, top, right, bottom;
    vLayout->getContentsMargins(&left, &top, &right, &bottom);
    vLayout->setContentsMargins(left, 0.0, right, 0.0);

    MLabel *label = new MLabel(m_stopLocation->name, this);
    label->setStyleName("CommonHeader");
    label->setTextElide(true);
    vLayout->addItem(label);

    m_progressbar = new MProgressIndicator(this, MProgressIndicator::barType);
    m_progressbar->setUnknownDuration(true);
    // m_progressbar->setVisible(false); /// not necessary, as will be used right away
    vLayout->addItem(m_progressbar);

    MPannableViewport *pv = new MPannableViewport(this);
    vLayout->addItem(pv);
    vLayout->setStretchFactor(pv, 100);
    m_departureBoard = new DepartureBoard(pv);
    pv->setWidget(m_departureBoard);
}

DeparturePage::DeparturePage(QSharedPointer<StopLocation> &stopLocation)
    : MApplicationPage(), m_stopLocation(stopLocation)
{
    setStyleName("CommonApplicationPage");
    setTitle(stopLocation->name);
    setPannable(false);

    m_departureWidget = new DepartureWidget(m_stopLocation, this);
    setCentralWidget(m_departureWidget);

    MAction *actionRefreshRequested = new MAction("icon-m-toolbar-refresh", QLatin1String("Refresh"), this);
    actionRefreshRequested->setLocation(MAction::ToolBarLocation);
    addAction(actionRefreshRequested);
    connect(actionRefreshRequested, SIGNAL(triggered()), m_departureWidget, SLOT(refreshRequested()));
}

DeparturePage::~DeparturePage()
{
    // nothing
}

void DeparturePage::createContent()
{
    m_departureWidget->createContent();
}
