/*****************************************************************************
 *
 * This file is part of MeeVaTr, a Meego/Harmattan program to fetch
 * time tables from the West-Swedish VästTrafik public transportation system.
 *
 *  Copyright (C) 2012  Thomas Fischer <fischer@unix-ag.uni-kl.de>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef DEPARTURE_H
#define DEPARTURE_H

#include <QDateTime>

#include <MApplicationPage>

#include "stoplocation.h"

class QNetworkAccessManager;

class MProgressIndicator;

class DepartureBoard;

typedef struct Departure {
    QString lineName, transportationType, directionName;
    QString fgColor, bgColor;
    QDateTime departureTime;
    bool booking;
};

Q_DECLARE_METATYPE(QSharedPointer<Departure>)

class DepartureWidget : public MWidget
{
    Q_OBJECT
public:
    DepartureWidget(QSharedPointer<StopLocation> &stopLocation, QGraphicsItem *parent = NULL);
    virtual void createContent();

public slots:
    void refreshRequested();

private slots:
    void searchFinished();
    void delayedInit();

private:
    QNetworkAccessManager *m_networkAccessManager;
    bool m_networkBusy;
    DepartureBoard *m_departureBoard;
    QSharedPointer<StopLocation> m_stopLocation;
    MProgressIndicator *m_progressbar;

    void setupGUI();
};

class DeparturePage : public MApplicationPage
{
    Q_OBJECT

public:
    DeparturePage(QSharedPointer<StopLocation> &stopLocation);
    ~DeparturePage();
    virtual void createContent();

private:
    DepartureWidget *m_departureWidget;
    QSharedPointer<StopLocation> m_stopLocation;
};

#endif // DEPARTURE_H
