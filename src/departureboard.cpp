/*****************************************************************************
 *
 * This file is part of MeeVaTr, a Meego/Harmattan program to fetch
 * time tables from the West-Swedish VästTrafik public transportation system.
 *
 *  Copyright (C) 2012  Thomas Fischer <fischer@unix-ag.uni-kl.de>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QFile>
#include <QIcon>

#include <QtXml/QDomDocument>

#include <QtSvg/QSvgRenderer>

#include <MDetailedListItem>
#include <MImageWidget>

#include "stoplocationlist.h"
#include "stoplocation.h"
#include "departure.h"
#include "departureboard.h"

const int DepartureBoard::DirectionNameRole = Qt::UserRole + 4675;
const int DepartureBoard::ConnectionLineNameRole = Qt::UserRole + 4676;
const int DepartureBoard::TimeRole = Qt::UserRole + 4677;
const int DepartureBoard::ForegroundColorStringRole = Qt::UserRole + 4678;
const int DepartureBoard::BackgroundColorStringRole = Qt::UserRole + 4679;
const int DepartureBoard::TransportationTypeRole = Qt::UserRole + 4680;
const int DepartureBoard::BookingRole = Qt::UserRole + 4681;

class DepartureBoardCellCreator : public MAbstractCellCreator<MDetailedListItem>
{
private:
    QGraphicsItem *m_p;

public:
    DepartureBoardCellCreator(QGraphicsItem *parent)
        : MAbstractCellCreator<MDetailedListItem>(), m_p(parent) {
    }

    QPixmap *transportationIcon(const QString &fgColorHex, const QString &bgColorHex, const QString &transportationType, bool booking, const int size) const {
        QPixmap *pixmapIcon = new QPixmap(size, size);
        pixmapIcon->fill(Qt::transparent);
        QPainter painterIcon(pixmapIcon);
        painterIcon.setRenderHints(QPainter::Antialiasing | QPainter::SmoothPixmapTransform, true);

        QString svgFilename;
        if (QLatin1String("BUS") == transportationType)
            svgFilename = QLatin1String(":/icons/bus.svg");
        else if (QLatin1String("TRAM") == transportationType)
            svgFilename = QLatin1String(":/icons/tram.svg");
        else if (QLatin1String("BOAT") == transportationType)
            svgFilename = QLatin1String(":/icons/boat.svg");
        else if (QLatin1String("TAXI") == transportationType)
            svgFilename = QLatin1String(":/icons/taxi.svg");
        else if (QLatin1String("VAS") == transportationType || QLatin1String("REG") == transportationType || QLatin1String("LOC") == transportationType)
            svgFilename = QLatin1String(":/icons/rail.svg");
        else
            svgFilename = QLatin1String(":/icons/unknowntransportation.svg");
        QFile svgFile(svgFilename);

        QByteArray svgByteArray;
        if (svgFile.open(QFile::ReadOnly)) {
            svgByteArray = svgFile.readAll();
            svgFile.close();

            svgByteArray = svgByteArray.replace("#000000", fgColorHex.toAscii());
            svgByteArray = svgByteArray.replace("#ffffff", bgColorHex.toAscii());
            QSvgRenderer svgTransportation(svgByteArray);

            QSize svgSize = svgTransportation.defaultSize();
            svgSize.scale(size * 3 / 4, size * 3 / 4, Qt::KeepAspectRatio);
            QPixmap pixmapSvg(svgSize);
            pixmapSvg.fill(Qt::transparent);
            QPainter painterSvg(&pixmapSvg);
            painterSvg.setRenderHints(QPainter::Antialiasing | QPainter::SmoothPixmapTransform, true);
            svgTransportation.render(&painterSvg);

            const int x = (pixmapIcon->width() - pixmapSvg.width()) / 2;
            const int y = (pixmapIcon->height() - pixmapSvg.height()) / 2;
            painterIcon.drawPixmap(x, y, pixmapSvg);
        }

        if (booking) {
            QSvgRenderer svgPhone(QLatin1String(":/icons/phone.svg"));
            QSize svgSize = svgPhone.defaultSize();
            svgSize.scale(size / 2, size / 2, Qt::KeepAspectRatio);
            QPixmap pixmapSvg(svgSize);
            pixmapSvg.fill(Qt::transparent);
            QPainter painterSvg(&pixmapSvg);
            painterSvg.setRenderHints(QPainter::Antialiasing | QPainter::SmoothPixmapTransform, true);
            svgPhone.render(&painterSvg);
            painterIcon.drawPixmap(0, 0, pixmapSvg);
        }

        return pixmapIcon;
    }

    MWidget *createCell(const QModelIndex &index, MWidgetRecycler &recycler) const {
        Q_UNUSED(index);

        MDetailedListItem *cell = dynamic_cast<MDetailedListItem *>(recycler.take(MDetailedListItem::staticMetaObject.className()));
        if (cell == NULL) {
            cell = new MDetailedListItem(MDetailedListItem::IconTitleSubtitleAndTwoSideIcons);
            cell->initLayout();
            cell->setLayoutPosition(M::CenterPosition);
        }
        updateCell(index, cell);

        return cell;
    }

    void updateCell(const QModelIndex &index, MWidget *cell) const {
        MDetailedListItem *item = qobject_cast<MDetailedListItem *>(cell);
        if (!item)
            return;

        QString connectionLineName = index.data(DepartureBoard::ConnectionLineNameRole).toString();
        /// do some internationalizations and beautifications
        connectionLineName = connectionLineName.replace(QLatin1String("Buss"), QLatin1String("Bus")).replace(QLatin1String("Expbuss"), QLatin1String("Exp Bus")).replace(QLatin1String("EXPRESS"), QLatin1String("EXPR")).replace(QString::fromUtf8("Spårvagn"), QLatin1String("Tram")).replace(QString::fromUtf8("Färja"), QLatin1String("Ferry"));
        static const QRegExp beautifyConnectionLineName(QLatin1String("([0-9].*$)"));
        connectionLineName = connectionLineName.replace(beautifyConnectionLineName, QLatin1String("<span style=\"font-weight:bold;\">\\1</span>"));
        const QDateTime timeDate = index.data(DepartureBoard::TimeRole).value<QDateTime>();
        QString subtitle;
        const int daysTo = QDateTime::currentDateTime().daysTo(timeDate);
        switch (daysTo) {
        case 0:
            subtitle = QString("%1 at <span style=\"font-weight:bold;\">%2</span>").arg(connectionLineName).arg(timeDate.toString("hh:mm"));
            break;
            /*case 1:
                subtitle = QString("%1, <span style=\"font-weight:bold;\">tomorrow</span> at <span style=\"font-weight:bold;\">%2</span>").arg(connectionLineName).arg(timeDate.toString("hh:mm"));
                break;*/
        default:
            subtitle = QString("%1 on <span style=\"font-weight:bold;\">%2</span> at <span style=\"font-weight:bold;\">%3</span>").arg(connectionLineName).arg(timeDate.toString("MMM d")).arg(timeDate.toString("hh:mm"));
        }
        const int secsTo = QTime::currentTime().secsTo(timeDate.time()) + daysTo * 86400;
        const int minsTo = (secsTo + 30) / 60;
        const int hrsTo = (minsTo + 30) / 60;
        if (minsTo == 0)
            subtitle += QLatin1String(" (<span style=\"font-weight:bold;\">now</span>)");
        else if (minsTo < 100)
            subtitle += QString(QLatin1String(" (in <span style=\"font-weight:bold;\">%1</span> min)")).arg(minsTo);
        /*else if (hrsTo < 100)
            subtitle += QString(QLatin1String(" (in <span style=\"color:#00A5DC;\">%1 hrs</span>)")).arg(hrsTo);*/

        QString destination = index.data(DepartureBoard::DirectionNameRole).toString();
        destination = destination.replace(QLatin1String(" via "), QLatin1String(" <span style=\"font-weight:normal;\">via</span> "));
        item->setTitle(destination);
        item->setSubtitle(subtitle);

        const QString fgColor = index.data(DepartureBoard::ForegroundColorStringRole).toString();
        const QString bgColor = index.data(DepartureBoard::BackgroundColorStringRole).toString();
        const QString transportationType = index.data(DepartureBoard::TransportationTypeRole).toString();
        const bool booking = index.data(DepartureBoard::BookingRole).toBool();
        item->setImageWidget(new MImageWidget(transportationIcon(fgColor, bgColor, transportationType, booking, item->boundingRect().height()), m_p));
    }
};

class DepartureBoardModel : public QAbstractListModel
{
private:
    QList<QSharedPointer<Departure> > m_departureList;

public:
    DepartureBoardModel(QObject *p = NULL)
        : QAbstractListModel(p) {
        // nothing
    }

    virtual void clear() {
        m_departureList.clear();
        reset();
    }

    void setXMLdata(const QString &xmlRawtext) {
        m_departureList.clear();

        QDomDocument doc("result");
        if (doc.setContent(xmlRawtext, false)) {
            QDomNodeList departureNodeList = doc.elementsByTagName("Departure");
            for (unsigned int i = 0; i < departureNodeList.length(); ++i) {
                QDomNamedNodeMap attributes = departureNodeList.item(i).attributes();
                Departure *departure = new Departure;
                departure->lineName = attributes.namedItem("name").toAttr().value();
                departure->transportationType = attributes.namedItem("type").toAttr().value();
                departure->directionName = attributes.namedItem("direction").toAttr().value();
                const QString time = attributes.namedItem("time").toAttr().value();
                const QString date = attributes.namedItem("date").toAttr().value();
                departure->departureTime = QDateTime(QDate::fromString(date, QLatin1String("yyyy-MM-dd")), QTime::fromString(time, QLatin1String("hh:mm")));
                departure->fgColor = attributes.namedItem("fgColor").toAttr().value();
                departure->bgColor = attributes.namedItem("bgColor").toAttr().value();
                departure->booking = QLatin1String("1") == attributes.namedItem("booking").toAttr().value();
                m_departureList << QSharedPointer<Departure>(departure);
            }

            reset();
        }
    }

    virtual int rowCount(const QModelIndex &parent = QModelIndex()) const {
        if (parent != QModelIndex())
            return 0;
        return m_departureList.count();
    }

    virtual QVariant data(const QModelIndex &index, int role) const {
        const int row = index.row();
        if (index == QModelIndex() || row < 0 || row >= m_departureList.count())
            return QVariant();

        switch (role) {
        case Qt::DisplayRole:
        case Qt::ToolTipRole:
        case DepartureBoard::DirectionNameRole:
            return m_departureList[row]->directionName;
        case DepartureBoard::TransportationTypeRole:
            return m_departureList[row]->transportationType;
        case DepartureBoard::ConnectionLineNameRole:
            return m_departureList[row]->lineName;
        case DepartureBoard::ForegroundColorStringRole:
            return m_departureList[row]->fgColor;
        case DepartureBoard::BackgroundColorStringRole:
            return m_departureList[row]->bgColor;
        case DepartureBoard::TimeRole:
            return m_departureList[row]->departureTime;
        case DepartureBoard::BookingRole:
            return m_departureList[row]->booking;
        default:
            return QVariant();
        }
    }

    virtual QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const {
        if (section != 0 || orientation != Qt::Horizontal || role != Qt::DisplayRole)
            return QVariant();

        return QLatin1String("Stop Location");
    }
};


DepartureBoard::DepartureBoard(QGraphicsItem *parent)
    : MList(parent)
{
    setCellCreator(new DepartureBoardCellCreator(this));
    m_model = new DepartureBoardModel(this);
    setItemModel(m_model);
    setSelectionMode(MList::NoSelection);
}

void DepartureBoard::setXMLdata(const QString &xmlRawtext)
{
    m_model->setXMLdata(xmlRawtext);
}

void DepartureBoard::clear()
{
    m_model->clear();
}
