/*****************************************************************************
 *
 * This file is part of MeeVaTr, a Meego/Harmattan program to fetch
 * time tables from the West-Swedish VästTrafik public transportation system.
 *
 *  Copyright (C) 2012  Thomas Fischer <fischer@unix-ag.uni-kl.de>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef DEPARTUREBOARD_H
#define DEPARTUREBOARD_H

#include <MList>

class DepartureBoardModel;

class DepartureBoard : public MList
{
    Q_OBJECT
public:
    static const int DirectionNameRole;
    static const int ConnectionLineNameRole;
    static const int TimeRole;
    static const int ForegroundColorStringRole;
    static const int BackgroundColorStringRole;
    static const int TransportationTypeRole;
    static const int BookingRole;

    DepartureBoard(QGraphicsItem *parent);

public slots:
    void setXMLdata(const QString &);
    void clear();

private:
    DepartureBoardModel *m_model;
};

#endif // DEPARTUREBOARD_H
