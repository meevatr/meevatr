/*****************************************************************************
 *
 * This file is part of MeeVaTr, a Meego/Harmattan program to fetch
 * time tables from the West-Swedish VästTrafik public transportation system.
 *
 *  Copyright (C) 2012  Thomas Fischer <fischer@unix-ag.uni-kl.de>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QGraphicsLinearLayout>
#include <QTextStream>
#include <QDebug>

#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkRequest>
#include <QtNetwork/QNetworkReply>

#include <MApplicationPage>
#include <MTextEdit>
#include <MButton>
#include <MPannableViewport>
#include <MMessageBox>
#include <MProgressIndicator>

#include "searchstoplocation.h"
#include "stoplocationlist.h"
#include "departure.h"

SearchStopLocationWidget::SearchStopLocationWidget(QGraphicsItem *parent)
    : MWidget(parent)
{
    m_networkAccessManager = new QNetworkAccessManager(this);
    setupGUI();
}

void SearchStopLocationWidget::setupGUI()
{
    QGraphicsLinearLayout *vLayout = new QGraphicsLinearLayout(Qt::Vertical, this);

    QGraphicsLinearLayout *hLayout = new QGraphicsLinearLayout(Qt::Horizontal);
    vLayout->addItem(hLayout);

    qreal left, top, right, bottom;
    vLayout->getContentsMargins(&left, &top, &right, &bottom);
    vLayout->setContentsMargins(left, 0.0, right, 0.0);

    m_lineEditQuery = new MTextEdit(MTextEditModel::SingleLine, QString(), this);
    // for development: m_lineEditQuery->setText(QString::fromUtf8("stensholmen"));
    m_lineEditQuery->setPrompt(QLatin1String("Enter stop's name"));
    hLayout->addItem(m_lineEditQuery);
    hLayout->setAlignment(m_lineEditQuery, Qt::AlignVCenter | Qt::AlignLeft);
    connect(m_lineEditQuery, SIGNAL(returnPressed()), this, SLOT(startSearch()));

    MButton *searchButton = new MButton(QLatin1String("icon-m-toolbar-search"), QString(), this);
    searchButton->setViewType(MButton::iconType);
    hLayout->addItem(searchButton);
    hLayout->setAlignment(searchButton, Qt::AlignVCenter | Qt::AlignRight);
    connect(searchButton, SIGNAL(clicked()), this, SLOT(startSearch()));

    m_progressbar = new MProgressIndicator(this, MProgressIndicator::barType);
    m_progressbar->setUnknownDuration(true);
    m_progressbar->setVisible(false);
    vLayout->addItem(m_progressbar);

    MPannableViewport *pv = new MPannableViewport(this);
    vLayout->addItem(pv);
    vLayout->setStretchFactor(pv, 100);
    m_stopLocationList = new StopLocationList(pv);
    pv->setWidget(m_stopLocationList);
    connect(m_stopLocationList, SIGNAL(itemClicked(const QModelIndex &)), this, SIGNAL(stationClicked(const QModelIndex &)));
}

void SearchStopLocationWidget::startSearch()
{
    const QString query = m_lineEditQuery->text();
    if (query.length() < 2) return;

    setEnabled(false);
    m_progressbar->setVisible(true);

    QUrl url("http://api.vasttrafik.se/bin/rest.exe/v1/location.name");
    url.addQueryItem("authKey", "47c5abaf-49d6-4c23-a1bd-b2e2766c4de7");
    url.addQueryItem("format", "xml");
    url.addQueryItem("input", query);
    QNetworkReply *reply = m_networkAccessManager->get(QNetworkRequest(url));
    connect(reply, SIGNAL(finished()), this, SLOT(searchFinished()));
}

void SearchStopLocationWidget::searchFinished()
{
    QNetworkReply *reply = static_cast<QNetworkReply *>(sender());

    m_progressbar->setVisible(false);

    if (reply->error() == QNetworkReply::NoError) {
        QTextStream ts(reply->readAll());
        ts.setCodec("UTF-8");
        const QString xml = ts.readAll();
        m_stopLocationList->setXMLdata(xml);
    } else {
        m_stopLocationList->clear();
        qWarning() << "Search Failed: error code" << reply->error();
        MMessageBox *mb = new MMessageBox(QLatin1String("Search Failed"), QString::fromUtf8("Could not search VästTrafik.\n\nError code: %1").arg(reply->error()), M::CancelButton);
        mb->exec();
    }
    setEnabled(true);
}


SearchStopLocation::SearchStopLocation()
    : MApplicationPage()
{
    setTitle("Search Stop Location");
    setPannable(false);

    SearchStopLocationWidget *widget = new SearchStopLocationWidget(this);
    setCentralWidget(widget);
    connect(widget, SIGNAL(stationClicked(const QModelIndex &)), this, SLOT(stationClicked(const QModelIndex &)));
}

void SearchStopLocation::createContent()
{
}

SearchStopLocation::~SearchStopLocation()
{
}

void SearchStopLocation::stationClicked(const QModelIndex &index)
{
    QSharedPointer<StopLocation> stopLocation = index.data(StopLocationList::StopLocationRole).value<QSharedPointer<StopLocation> >();
    MApplicationPage *departurePage = new DeparturePage(stopLocation);
    departurePage->appear(scene(), MSceneWindow::DestroyWhenDismissed);
}
