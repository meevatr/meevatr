/*****************************************************************************
 *
 * This file is part of MeeVaTr, a Meego/Harmattan program to fetch
 * time tables from the West-Swedish VästTrafik public transportation system.
 *
 *  Copyright (C) 2012  Thomas Fischer <fischer@unix-ag.uni-kl.de>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef SEARCHSTOPLOCATION_H
#define SEARCHSTOPLOCATION_H

#include <QModelIndex>

#include <MApplicationPage>

class QNetworkAccessManager;

class MTextEdit;
class MProgressIndicator;

class StopLocationList;

class SearchStopLocationWidget : public MWidget
{
    Q_OBJECT
public:
    SearchStopLocationWidget(QGraphicsItem *parent = NULL);

signals:
    void stationClicked(const QModelIndex &);

private slots:
    void startSearch();
    void searchFinished();

private:
    QNetworkAccessManager *m_networkAccessManager;
    StopLocationList *m_stopLocationList;
    MTextEdit *m_lineEditQuery;
    MProgressIndicator *m_progressbar;

    void setupGUI();
};

class SearchStopLocation : public MApplicationPage
{
    Q_OBJECT

public:
    SearchStopLocation();
    ~SearchStopLocation();
    virtual void createContent();

private slots:
    void stationClicked(const QModelIndex &);
};

#endif // SEARCHSTOPLOCATION_H
