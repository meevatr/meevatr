TEMPLATE = app
TARGET = "meevatr"
CONFIG += meegotouch
DEPENDPATH += .
INCLUDEPATH += .

RESOURCES += icons.qrc

HEADERS += searchstoplocation.h stoplocationlist.h departure.h departureboard.h
SOURCES += main.cpp searchstoplocation.cpp stoplocationlist.cpp departure.cpp departureboard.cpp

unix {
    PREFIX = /opt/meevatr
    BINDIR = $$PREFIX/bin

    INSTALLS += target
    target.path =$$BINDIR
}
