/*****************************************************************************
 *
 * This file is part of MeeVaTr, a Meego/Harmattan program to fetch
 * time tables from the West-Swedish VästTrafik public transportation system.
 *
 *  Copyright (C) 2012  Thomas Fischer <fischer@unix-ag.uni-kl.de>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QIcon>
#include <QDebug>

#include <QtXml/QDomDocument>

#include <MBasicListItem>
#include <MImageWidget>
#include <MAction>

#include "stoplocationlist.h"
#include "stoplocation.h"

const int StopLocationList::StopLocationNameRole = Qt::UserRole + 7820;
const int StopLocationList::StopLocationIdRole = Qt::UserRole + 7821;
const int StopLocationList::StopLocationLongitudeRole = Qt::UserRole + 7822;
const int StopLocationList::StopLocationLatitudeRole = Qt::UserRole + 7823;
const int StopLocationList::StopLocationRole = Qt::UserRole + 7824;
// TODO const int StopLocationList::StopLocationIsFavoriteRole = Qt::UserRole + 7825;

class StopLocationCellCreator : public MAbstractCellCreator<MBasicListItem>
{
public:
    StopLocationCellCreator() : MAbstractCellCreator<MBasicListItem>() {
        // nothing
    }

    MWidget *createCell(const QModelIndex &index, MWidgetRecycler &recycler) const {
        Q_UNUSED(index);

        MBasicListItem *cell = dynamic_cast<MBasicListItem *>(recycler.take(MBasicListItem::staticMetaObject.className()));
        if (cell == NULL) {
            cell = new MBasicListItem(/* TODO MBasicListItem::IconWithTitle */  MBasicListItem::SingleTitle);
            cell->initLayout();
            cell->setLayoutPosition(M::CenterPosition);

            /* TODO
            MAction *action = new MAction(cell);
            action->setData(QLatin1String("IS_FAVORITE"));
            cell->addAction(action);
            action->setLocation(MAction::ObjectMenuLocation);
            */
        }
        updateCell(index, cell);

        return cell;
    }

    void updateCell(const QModelIndex &index, MWidget *cell) const {
        // TODO static const QPixmap *pixmapIsFavorite = MTheme::pixmap("icon-s-common-favorite-mark");
        // TODO static const QPixmap *pixmapIsNotFavorite = MTheme::pixmap("icon-s-common-favorite-unmark");

        MBasicListItem *item = qobject_cast<MBasicListItem *>(cell);
        if (!item)
            return;

        item->setTitle(index.data(StopLocationList::StopLocationNameRole).toString());

        /* TODO
        bool isFavorite = index.data(StopLocationList::StopLocationIsFavoriteRole).toBool();
        const QPixmap *chosenIcon = isFavorite ? pixmapIsFavorite : pixmapIsNotFavorite;
        const int height = 64;
        QPixmap pixmapFavorite(height, height);
        pixmapFavorite.fill(Qt::transparent);
        QPainter p(&pixmapFavorite);
        const int x = (pixmapFavorite.width() - chosenIcon->width()) / 2;
        const int y = (pixmapFavorite.height() - chosenIcon->height()) / 2;
        p.drawPixmap(x, y, *chosenIcon);
        item->imageWidget()->setPixmap(pixmapFavorite);

        for (QList<QAction *>::Iterator it = item->actions().begin(); it != item->actions().end(); ++it) {
            if ((*it)->data() == QLatin1String("IS_FAVORITE")) {
                bool isFavorite = index.data(StopLocationList::StopLocationIsFavoriteRole).toBool();
                (*it)->setText(isFavorite ? QLatin1String("Unmark as favorite") : QLatin1String("Mark as favorite"));
            }
        }
        */
    }
};

class StopLocationListModel : public QAbstractListModel
{
private:
    QList<QSharedPointer<StopLocation> > m_stopLocationList;

public:
    StopLocationListModel(QObject *p = NULL)
        : QAbstractListModel(p) {
        // nothing
    }

    virtual void clear() {
        m_stopLocationList.clear();
        reset();
    }

    void setXMLdata(const QString &xmlRawtext) {
        m_stopLocationList.clear();

        QDomDocument doc("result");
        if (doc.setContent(xmlRawtext, false)) {
            QDomNodeList locationList = doc.elementsByTagName("StopLocation");
            for (unsigned int i = 0; i < locationList.length(); ++i) {
                QDomNamedNodeMap attributes = locationList.item(i).attributes();
                StopLocation *stopLocation = new StopLocation;
                stopLocation->id = attributes.namedItem("id").toAttr().value().toULongLong();
                stopLocation->name = attributes.namedItem("name").toAttr().value();
                stopLocation->longitude = attributes.namedItem("lon").toAttr().value().toDouble();
                stopLocation->latitude = attributes.namedItem("lat").toAttr().value().toDouble();
                // TODO stopLocation->isFavorite = false;
                m_stopLocationList << QSharedPointer<StopLocation>(stopLocation);
            }

            reset();
        }
    }

    virtual int rowCount(const QModelIndex &parent = QModelIndex()) const {
        if (parent != QModelIndex())
            return 0;
        return m_stopLocationList.count();
    }

    virtual QVariant data(const QModelIndex &index, int role) const {
        const int row = index.row();
        if (index == QModelIndex() || row < 0 || row >= m_stopLocationList.count())
            return QVariant();

        switch (role) {
        case Qt::DisplayRole:
        case Qt::ToolTipRole:
        case StopLocationList::StopLocationNameRole:
            return m_stopLocationList[row]->name;
        case StopLocationList::StopLocationIdRole:
            return m_stopLocationList[row]->id;
        case StopLocationList::StopLocationLatitudeRole:
            return m_stopLocationList[row]->latitude;
        case StopLocationList::StopLocationLongitudeRole:
            return m_stopLocationList[row]->longitude;
        case StopLocationList::StopLocationRole:
            return QVariant::fromValue(m_stopLocationList[row]);
            /* TODO
            case StopLocationList::StopLocationIsFavoriteRole:
                return m_stopLocationList[row]->isFavorite;
            */
        default:
            return QVariant();
        }
    }

    /* TODO
    virtual bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) {
        const int row = index.row();
        if (index == QModelIndex() || row < 0 || row >= m_stopLocationList.count() || !value.canConvert<bool>())
            return false;

        m_stopLocationList[row]->isFavorite = value.toBool();
        emit dataChanged(index, index);
        return true;
    }
    */

    virtual QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const {
        if (section != 0 || orientation != Qt::Horizontal || role != Qt::DisplayRole)
            return QVariant();

        return QLatin1String("Stop Location");
    }
};

StopLocationList::StopLocationList(QGraphicsItem *parent)
    : MList(parent)
{
    setCellCreator(new StopLocationCellCreator());
    m_model = new StopLocationListModel(this);
    setItemModel(m_model);
}

void StopLocationList::setXMLdata(const QString &xmlRawtext)
{
    m_model->setXMLdata(xmlRawtext);
}

void StopLocationList::clear()
{
    m_model->clear();
}
